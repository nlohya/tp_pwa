import { Component } from '@angular/core';
import { Device } from '@capacitor/device';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  private model: String;
  private platform: String;
  private os: String;
  private osVersion: String;
  private manufacturer: String;

  constructor() {
    this.logDeviceInfo();
    this.model = "";
    this.platform = "";
    this.os = "";
    this.osVersion = "";
    this.manufacturer = "";
  }

  private logDeviceInfo = async () => {
    const info = await Device.getInfo();

    this.model = info.model;
    this.platform = info.platform;
    this.os = info.operatingSystem;
    this.osVersion = info.osVersion;
    this.manufacturer = info.manufacturer;
  };

  public getModel(): String {
    if (this.model)
      return this.model;
    else
      return "inconnu";
  }

  public getPlatform(): String {
    if (this.platform)
      return this.platform;
    else
      return "inconnu";
  }

  public getOs(): String {
    if (this.os)
      return this.os;
    else
      return "inconnu";
  }

  public getOsVersion(): String {
    if (this.osVersion)
      return this.osVersion;
    else
      return "inconnu";
  }

  public getManufacturer(): String {
    if (this.manufacturer)
      return this.manufacturer;
    else
      return "inconnu";
  }

}
