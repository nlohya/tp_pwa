import { Component } from '@angular/core';
import { Geolocation } from '@capacitor/geolocation';
import { Preferences } from '@capacitor/preferences';
import { GPSPos } from './interfaces/gps';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  private posList: Array<GPSPos>;

  constructor() {
    this.posList = [];
  }

  ngOnInit() {
    this.watchCurrentPosition();
    this.retrieveStoredPositions();
  }

  private async retrieveStoredPositions() {
    const { value } = await Preferences.get({key: 'positions'});
    if (value) {
      this.posList = JSON.parse(value);
    }
  }

  public getPosList(): Array<GPSPos> {
    return this.posList;
  }

  public shouldDisplayPositions() {
    return this.posList.length > 0;
  }

  private watchCurrentPosition() {
    Geolocation.watchPosition({
      enableHighAccuracy: true,
      timeout: 1000,
    }, (position) => {
      if (position) {
        this.addPositionToList({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          timestamp: position.timestamp
        });
        this.savePositions()
      }
    });
  }

  private async addPositionToList(pos: GPSPos) {
    if (this.posList.length < 5) {
      this.posList.push(pos);
    } else {
      this.posList.shift();
      this.posList.push(pos);
    }
  }

  private async savePositions() {
    await Preferences.set({
      key: 'positions',
      value: JSON.stringify(this.posList) 
    })
  }

}
