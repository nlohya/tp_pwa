export interface GPSPos {
    latitude: number
    longitude: number
    timestamp: number
}