export interface Alert {
    id: number
    title: String
    text: String
    date: Date
}