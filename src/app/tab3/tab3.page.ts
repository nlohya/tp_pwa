import { Component } from '@angular/core';
import { Alert } from './interfaces/alert';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  private alertList: Array<Alert>;

  public minDate: any = new Date().toISOString();

  public title: String;
  public desc: String;
  public date: Date;

  constructor() {
    this.alertList = []
    this.title = ""
    this.desc = ""
    this.date = new Date()
  }

  public createAlert() {
    if (this.alertList.length == 0) {
      this.alertList.push({
        id: 0,
        title: this.title,
        text: this.desc,
        date: this.date
      })
      this.resetFields()
    } else {
      this.alertList.push({
        id: this.alertList[this.alertList.length - 1].id + 1,
        title: this.title,
        text: this.desc,
        date: this.date
      })
      this.resetFields()
    }
  }

  private resetFields() {
    this.title = ""
    this.desc = ""
    this.date = new Date()
  }

  public getAlerts() {
    return this.alertList
  }
}
